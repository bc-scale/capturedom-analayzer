from pathlib import Path

# =========== Python Filesystem =========== #
APP_ROOT = str(Path(__file__).parent.parent)
CAPTURE_DOM_FOLDER = str(Path(__file__).parent.parent.parent)
FINDER_PATH = "/XSS Tools"
CHROMIUM_TAINT_TRACKING_FOLDER = '/chromium_taint_tracking'
CHROMIUM_TAINT_TRACKING_PATH = APP_ROOT+FINDER_PATH + CHROMIUM_TAINT_TRACKING_FOLDER

# =========== CaptureDOM links =========== #
root = "http://localhost:8001"
levels_page = "http://localhost:8001/levels.html"
DOM_levels_page = "http://localhost:8001/DOM/dom.html"
REFLECTED_levels_page = "http://localhost:8001/REFLECTED/reflected.html"
STORED_levels_page = "http://localhost:8001/STORED/stored.html"
SETTINGS_page = "http://localhost:8001/setting.php"

# =========== CaptureDOM DOM level links =========== #
DOM_low_level = "http://localhost:8001/DOM/low/brand_cars_list.html"
DOM_medium_level = "http://localhost:8001/DOM/medium/countries.php"
DOM_high_level = "http://localhost:8001/DOM/high/languages.php"

# =========== CaptureDOM REFLECTED level links =========== #
REFLECTED_low_level = "http://localhost:8001/REFLECTED/low/sayhello.php"
REFLECTED_medium_level = "http://localhost:8001/REFLECTED/medium/sayhello.php"
REFLECTED_high_level = "http://localhost:8001/REFLECTED/high/sayhello.php"

# =========== CaptureDOM STORED level links =========== #
STORED_low_level = "http://localhost:8001/STORED/low/cv_presentation.php"
STORED_medium_level = "http://localhost:8001/STORED/medium/cv_presentation.php"
STORED_high_level = "http://localhost:8001/STORED/high/cv_presentation.php"

levels = [DOM_low_level, DOM_medium_level, DOM_high_level, REFLECTED_low_level, REFLECTED_medium_level,
          REFLECTED_high_level, STORED_low_level, STORED_medium_level, STORED_high_level]
DOM_level = [DOM_low_level, DOM_medium_level, DOM_high_level]
levels_pages = [DOM_levels_page, REFLECTED_levels_page, STORED_levels_page]
only_paths = ["http://localhost:8001/STORED", "http://localhost:8001/REFLECTED", "http://localhost:8001/DOM"]
levels_and_params = ['http://localhost:8001/DOM/low/brand_cars_list.html?default=Abarth',
                     'http://localhost:8001/DOM/medium/countries.php?default=Afghanistan',
                     'http://localhost:8001/DOM/high/languages.php?default=English',
                     'http://localhost:8001/REFLECTED/low/sayhello.php?name=alice',
                     'http://localhost:8001/REFLECTED/medium/sayhello.php?name=alice',
                     'http://localhost:8001/REFLECTED/high/sayhello.php?name=alice',
                     'http://localhost:8001/STORED/low/cv_presentation.php --data="txtName=test&mtxMessage=tets"',
                     'http://localhost:8001/STORED/medium/cv_presentation.php --data="txtName=test&mtxMessage=tets"',
                     'http://localhost:8001/STORED/high/cv_presentation.php --data="txtName=test&mtxMessage=tets"']

levels_for_xsser = ["http://localhost:8001/DOM/low -g'/brand_cars_list.html?default=XSS'",
                    "http://localhost:8001/DOM/medium -g'/countries.php??default=XSS'",
                    "http://localhost:8001/DOM/high -g 'languages.php?default=XSS'",
                    "http://localhost:8001/REFLECTED/low -g '/sayhello.php?name=XSS'",
                    "http://localhost:8001/REFLECTED/medium -g'/sayhello.php?name=XSS'",
                    "http://localhost:8001/REFLECTED/high -g '/sayhello.php?name=XSS'",
                    "http://localhost:8001STORED/low/cv_presentation.php -p='txtName=test&mtxMessage=XSS'",
                    "http://localhost:8001/STORED/medium/cv_presentation.php -p='txtName=test&mtxMessage=XSS'",
                    "http://localhost:8001/STORED/high/cv_presentation.php -p='txtName=test&mtxMessage=XSS'"]

CSV_RESULT = "results.csv"
CSV_TIME = "results_time.csv"
CSV_TYPE = "results_type.csv"
CSV_HEADER = ['Tool name', 'DOM low', 'DOM medium', 'DOM high',
              'REFLECTED low', 'REFLECTED medium', 'REFLECTED high',
              'STORED low', 'STORED medium', 'STORED high']

CSV_HEADER_MOD = ['DOM low', 'DOM medium', 'DOM high',
                  'REFLECTED low', 'REFLECTED medium', 'REFLECTED high',
                  'STORED low', 'STORED medium', 'STORED high']