import socket
import csv
from utils.settings import APP_ROOT, CSV_RESULT, CSV_HEADER, CSV_TIME, CSV_TYPE


def get_local_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


def create_csv():
    with open(APP_ROOT + '/'+CSV_RESULT, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(CSV_HEADER)
    f.close()
    with open(APP_ROOT + '/'+CSV_TIME, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(CSV_HEADER)
    f.close()
    with open(APP_ROOT + '/' + CSV_TYPE, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(CSV_HEADER)
    f.close()


def save_into_csv(row, csv_file):
    with open(APP_ROOT + '/' + csv_file, 'a') as f:
        writer = csv.writer(f)
        writer.writerow(row)
    f.close()
