import os
import time
import json

from utils.settings import CHROMIUM_TAINT_TRACKING_PATH, CSV_RESULT, CSV_TIME, CSV_TYPE
from utils.utility_functions import get_local_ip, save_into_csv
import requests

from cleantext import clean


def XSSstrike_parser(levels):
    print("########### XSStrike ############")
    in_csv = ['XSStrike']
    in_csv_time = ['XSStrike']
    in_csv_vuln_type = ['XSStrike']

    for level in levels:
        founded = False
        dom = False
        reflected = False
        stored = False

        print(level)
        start_time = time.time()
        logs = os.popen("docker exec XSStrike python xsstrike.py  -u " +
                        level.replace('localhost', get_local_ip()) + " --timeout=2").read()
        result_time = time.time() - start_time
        in_csv_time.append(result_time)
        print(logs)
        results = [clean(line, lower=False, no_currency_symbols=True, no_punct=True, replace_with_currency_symbol='').replace('~', '').replace("+", '').rstrip().lstrip() for line in logs.splitlines()]

        if 'Potentially vulnerable objects found' in results and "No reflection found" in results:
            print('Result:', 'FOUNDED\n')
            founded = True
            dom = True
        elif 'Reflections found 1' in results:
            print('Result:', 'FOUNDED \n')
            founded = True
            reflected = True

        if founded is True:
            in_csv.append('founded')
        else:
            in_csv.append('not founded')
            print('Result:', 'NOT FOUNDED\n')

        if dom is True:
            in_csv_vuln_type.append('DOM')
        elif reflected is True:
            in_csv_vuln_type.append('REFLECTED')
        elif stored is True:
            in_csv_vuln_type.append('STORED')
        else:
            in_csv_vuln_type.append('Undefined')

    save_into_csv(in_csv, CSV_RESULT)
    save_into_csv(in_csv_time, CSV_TIME)
    save_into_csv(in_csv_vuln_type, CSV_TYPE)


def XSS_scanner_parser(levels):
    print("########### XSS Scanner ############")
    in_csv = ['XSS scanner']
    in_csv_time = ['XSS scanner']
    in_csv_vuln_type = ['XSS scanner']

    for level in levels:
        print(level)
        payload = {"url": level}
        start_time = time.time()

        try:
            r = requests.get("http://localhost:4000/scanner", params=payload)
            result_time = time.time() - start_time
            in_csv_time.append(result_time)
            print(r.text)
            if r.text:
                in_csv.append('founded')
                in_csv_vuln_type.append('Undefined')
            elif r.text is False:
                in_csv.append('not founded')
                in_csv_vuln_type.append('Undefined')
            time.sleep(15)
        except Exception as e:
            print(e)
            in_csv.append('not determinate')
            continue

    save_into_csv(in_csv, CSV_RESULT)
    save_into_csv(in_csv_time, CSV_TIME)
    save_into_csv(in_csv_vuln_type, CSV_TYPE)


def dsxs_parser(levels):
    print("########### DSXS ############")

    in_csv = ['dsxs']
    in_csv_time = ['dsxs']
    in_csv_vuln_type = ['dsxs']

    for level in levels:
        start_time = time.time()
        logs = os.popen("docker exec DSXS python dsxs.py -u " +
                        level.replace('localhost', get_local_ip())).read()
        result_time = time.time() - start_time
        in_csv_time.append(result_time)
        results = logs.splitlines()
        if 'no vulnerabilities found' in results[len(results) - 1]:
            in_csv.append('not founded')
            in_csv_vuln_type.append('Undefined')
        elif 'possible vulnerabilities found' in results[len(results) - 1]:
            in_csv.append('founded')
            in_csv_vuln_type.append('Undefined')

    save_into_csv(in_csv, CSV_RESULT)
    save_into_csv(in_csv_time, CSV_TIME)
    save_into_csv(in_csv_vuln_type, CSV_TYPE)


def XSpear_parser(levels):
    print("########### XSpear ############")

    in_csv = ['XSpear']
    in_csv_time = ['XSpear']
    in_csv_vuln_type = ['XSpear']

    for level in levels:
        founded = False
        print("Level: ", level)
        start_time = time.time()
        logs = os.popen("docker exec XSpear XSpear -u" + level.replace('localhost', get_local_ip()) +
                        '-v 2 -o json').read()
        print(logs)
        result_time = time.time() - start_time
        in_csv_time.append(result_time)
        results = logs.splitlines()
        results_json = json.loads(results[len(results) - 1])
        issues_list = results_json['issue_list']
        for issue in issues_list:
            if 'XSS' in issue['issue']:
                founded = True
        if founded is True:
            in_csv.append('founded')
            in_csv_vuln_type.append('Undefined')
        else:
            in_csv.append('not founded')
            in_csv_vuln_type.append('Undefined')

    save_into_csv(in_csv, CSV_RESULT)
    save_into_csv(in_csv_time, CSV_TIME)
    save_into_csv(in_csv_vuln_type, CSV_TYPE)


def xsser_parser(levels):
    print("########### xsser ############")

    in_csv = ['xsser']
    in_csv_time = ['xsser']
    in_csv_vuln_type = ['xsser']

    for level in levels:
        print('Level: ', level)
        start_time = time.time()
        logs = os.popen("docker exec xsser ./xsser  -u" + level.replace('localhost', get_local_ip())).read()
        print("Log: \n", logs)
        result_time = time.time() - start_time
        in_csv_time.append(result_time)
        results = [line.replace("\n", '').replace('-', '').lstrip() for line in logs.split("Final Results:", 1)[1].replace("=", '').splitlines(True)]

        successful = int(str(results[5]).split('Successful: ')[1])
        if successful >= 1:
            in_csv.append('founded')
            in_csv_vuln_type.append('Undefined')
        else:
            in_csv.append('not founded')
            in_csv_vuln_type.append('Undefined')

    save_into_csv(in_csv, CSV_RESULT)
    save_into_csv(in_csv_time, CSV_TIME)
    save_into_csv(in_csv_vuln_type, CSV_TYPE)


def xss_sniper_parser(levels):
    print("########### sniper ############")

    in_csv = ['sniper']
    in_csv_time = ['sniper']
    in_csv_vuln_type = ['sniper']
    for level in levels:
        found = False
        start_time = time.time()
        level = level.replace('localhost', get_local_ip()).replace('--data', '--post --data')
        logs = os.popen(" docker exec xssniper python xsssniper.py -u" + level).read()
        print(logs)
        result = logs.splitlines()
        result_time = time.time() - start_time
        in_csv_time.append(result_time)
        for line in result:
            if 'RESULT: Found XSS' in line:
                found = True
                break

        if found is True:
            in_csv.append('founded')
            in_csv_vuln_type.append('Undefined')
        else:
            in_csv.append('not founded')
            in_csv_vuln_type.append('Undefined')

    save_into_csv(in_csv, CSV_RESULT)
    save_into_csv(in_csv_time, CSV_TIME)
    save_into_csv(in_csv_vuln_type, CSV_TYPE)


def pwnxss_parser(levels):
    print("########### PwnXSS ############")

    in_csv = ['PwnXSS']
    in_csv_time = ['PwnXSS']
    in_csv_vuln_type = ['PwnXSS']

    for level in levels:
        start_time = time.time()
        logs = os.popen("docker exec PwnXSS python pwnxss.py --single " +
                        level.replace('localhost', get_local_ip())).read()
        print(logs)
        result_time = time.time() - start_time
        in_csv_time.append(result_time)
        result = logs.splitlines()
        found = False
        for line in result:
            if 'Detected XSS' in line:
                found = True
                break
        if found is True:
            in_csv.append('founded')
            in_csv_vuln_type.append('Undefined')
        else:
            in_csv.append('not founded')
            in_csv_vuln_type.append('Undefined')

    save_into_csv(in_csv, CSV_RESULT)
    save_into_csv(in_csv_time, CSV_TIME)
    save_into_csv(in_csv_vuln_type, CSV_TYPE)


def chromium_taint_tracking_parser(levels):
    print("########### Chromium taint tracking ############")
    in_csv = ['ChromiumTaintTracking']
    in_csv_vuln_type = ['ChromiumTaintTracking']

    file = open(CHROMIUM_TAINT_TRACKING_PATH + '/out_concat.txt', 'r')
    file_lines = str(file.read()).splitlines()
    print(file_lines)
    XXS_founded = []
    for line in file_lines:
        if 'XSS' in line:
            XXS_founded.append(line)
    for level in levels:
        found = False
        for xss in XXS_founded:
            if level.replace('localhost', get_local_ip()) in xss:
                found = True
                break

        if found is True:
            print('FOUNDED')
            in_csv.append('founded')
            in_csv_vuln_type.append('Undefined')
        else:
            print("NOT FOUNDED")
            in_csv.append('not founded')
            in_csv_vuln_type.append('Undefined')

    save_into_csv(in_csv, CSV_RESULT)
    save_into_csv(in_csv_vuln_type, CSV_TYPE)
