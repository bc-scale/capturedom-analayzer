from utils.settings import *
from parsers import *
from utils.utility_functions import *
from datetime import datetime


if __name__ == "__main__":
    print("CaptureDOM folder path:", CAPTURE_DOM_FOLDER)
    print("LOCAL IP:", get_local_ip())
    create_csv()

    print("START PARSER")
    start_time = datetime.now().replace(microsecond=0)

    xsser_parser(levels_for_xsser)
    XSSstrike_parser(levels_and_params)
    XSS_scanner_parser(levels)
    XSpear_parser(levels_and_params)
    dsxs_parser(levels_and_params)
    xss_sniper_parser(levels_and_params)
    pwnxss_parser(levels)
    chromium_taint_tracking_parser([DOM_low_level, DOM_medium_level, DOM_high_level, REFLECTED_low_level,
                                    REFLECTED_medium_level, REFLECTED_high_level, STORED_low_level,
                                    STORED_medium_level, STORED_high_level])

    now = datetime.now().replace(microsecond=0)
    print("\nExecution time: \t")
    print((now - start_time).total_seconds(), 'seconds')